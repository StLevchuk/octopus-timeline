<?php 
    /* Template Name: Brighter */ 
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<h1>A brighter way</h1>
<div class="purpose">
  <div class="left">
    <div class="title">Our purpose</div>
      <div class="body">
        <div class="line">&nbsp;</div>
        <p>
          Most companies don’t put their customers first. And when it comes to the things that matter most to us in life, like financial security, health and wellbeing, that’s a big problem.
        </p>
        <p>
          We set up Octopus back in 2000 to change this. We set out to build businesses where our customers would feel so strongly about what we did, and how we did it, that they would proactively recommend us to those they were closest to. 
        </p>
        <p>
          So far our approach is working. We’re changing the way people think about their savings, we’re building care homes, hospitals and schools you’d be proud to send your loved ones to, and we’re supplying hundreds of thousands of UK homes with renewable energy. 
        </p>
        <p>
          And there’s much more to come. We call it ‘a brighter way’. 
          <br>
          Changing today for a better tomorrow.    
        </p>
      </div>
  </div> 
  <div class="right">
      <img src="./assets/images/brighter/purpose.jpg" />
  </div>
</div>
<div class="approach">
    <div class="left">
      <img src="./assets/images/brighter/approach.png">
    </div>
    <div class="right">
      <div class="title">
        Our approach
      </div>
      <div class="body">
        <div class="line">&nbsp;</div>
        <p>
          The average toddler asks 390 questions a day. But most people’s constant questioning slows down as they get older. That’s not us. </p>
        <p>
          We take the same approach in every market we enter. We start with the customer and keep asking questions until we’ve worked out what’s broken – and how we’re going to fix it.
        </p>
        <p>
          So, whichever Octopus business you deal with, your experience of us will be the same. We never stop learning and we’ll never settle for good enough.
        </p>
      </div>
    </div>
</div>
<div class="find-out">
  <div class="title">
    Find out what we can do for you
  </div>
  <span>
    Interested in hearing about our businesses?
  </span>
  <div>
    <a href="/our-businesses/" class="custom-button brighter-button">Learn more</a>
  </div>
</div>
<?php  get_footer(); 
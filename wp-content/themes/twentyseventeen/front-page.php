<?php

get_header(); ?>

	<header class="hero">
		<?php //include 'other_menu.php'; ?>
		<?php
		if ( is_front_page() || is_home() ):
			if ( ! wp_is_mobile() ):
				include( 'video.php' );
			else:
				include( 'mobile_video.php' );
			endif;
		endif;
		?>
		<div class="main-block">
			<div class="main-block_inner">
				<div class="main-block_title">
					<div class="title-one">Better</div>
					<div class="title-two">It's something everyone wants</div>
				</div>
				<div class="play-video_btn">
					<div class="play-video js-play"></div>
					<div class="play-video-text">Watch our film</div>
				</div>
			</div>
		</div>
		<div class="scroll-down">
			<div class="text">Scroll Down</div>
			<img src="/wp-content/themes/twentyseventeen/assets/images/arrow_down.png" alt="">
		</div>
	</header>

	<div class="site-content-contain">
	<div id="content" class="site-content">

	<div>
		<div class="main-description">
			<div class="left-description">
				<h1>Welcome to Octopus</h1>
				<p>
					What if there’s a brighter way to do business? At Octopus we’re obsessed with answering that question. And for
					the last 17 years it’s been leading us in some exciting directions.
				</p>
				<div class="sub-description">
				<span>We’ve grown rapidly by focusing on investments, energy and healthcare. We’re known for innovation, doing the simple things well and for 	customer service that goes the extra mile. 
				</span>
					<span>
					Because when you put people first, you can make life better for everyone. And that’s the way it should be.
				</span>
				</div>
				<a href="/about-us" class="custom-button description-button">Learn more</a>
			</div>
			<div class="right-image">
				<img src="./assets/images/legs.png"/>
			</div>
		</div>
		<div class="main-business">
			<h1>Our businesses</h1>
			<div class="business-items">

				<a href="/our-businesses#investments">
					<div id="trigger-one" class="business-item business-item-1">
						<div class="title business-item_company">
							<img class="octopus-sp-title"
									 src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_I_Primary_289 RGB.png"/>
						</div>
						<img src="/wp-content/themes/twentyseventeen/assets/images/investments.png" alt="Investments"/>
						<div>
						<span>
							Changing the world of investing for the 
							better, with simple, jargon-free products 
							that do what they're supposed to.
						</span>
							<span class="custom-button button-news button-arrow">Discover </span>
						</div>
					</div>
				</a>

				<a href="/our-businesses#healthcare">
					<div class="business-item business-item-2">
						<div class="title business-item_company">
							<img class="octopus-sp-title"
									 src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_H_Primary_289 RGB.png"/>
						</div>
						<img src="/wp-content/themes/twentyseventeen/assets/images/healthcare.png" alt="Healthcare"/>
						<div>
						<span>
							Improving people's health and wellbeing
							by investing in care homes, hospitals,
							surgeries and schools.
						</span>
							<span class="custom-button button-news button-arrow">Discover </span>
						</div>
					</div>
				</a>

				<a href="/our-businesses#ventures">
					<div class="business-item business-item-3">
						<div class="title business-item_company">
							<img class="octopus-sp-title"
									 src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_V_Primary_289 RGB.png"/>
						</div>
						<img src="/wp-content/themes/twentyseventeen/assets/images/ventures.png" alt="Ventures"/>
						<div>
						<span>
							Investing £100 million a year into unusually talented entrepreneurs intent on building big businesses.
						</span>
							<span class="custom-button button-news button-arrow">Discover </span>
						</div>
					</div>
				</a>

				<a href="/our-businesses#property">
					<div id="trigger-two" class="business-item business-item-4">
						<div class="title business-item_company">
							<img class="octopus-sp-title"
									 src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_P_Primary_289 RGB.png"/>
						</div>
						<img src="/wp-content/themes/twentyseventeen/assets/images/property.png" alt="Property"/>
						<div>
						<span>
							Providing fast and flexible loans to UK
							residential and commercial property
							developers and investors.
						</span>
							<span class="custom-button button-news button-arrow">Discover </span>
						</div>
					</div>
				</a>

				<a href="/our-businesses#energy">
					<div class="business-item business-item-5">
						<div class="title business-item_company">
							<img class="octopus-sp-title"
									 src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_E_Primary_289 RGB.png"/>
						</div>
						<div class="business-item_thumb"
								 style="background-image: url('/wp-content/themes/twentyseventeen/assets/images/energy.jpg')"
								 title="Energy"></div>
						<!--					<img src="/wp-content/themes/twentyseventeen/assets/images/energy.jpg" alt="Energy"/>-->
						<div>
						<span>
							Not only one of the UK's biggest
							generators of renewable energy, but also 
							one of its top-rated energy suppliers.
						</span>
							<span class="custom-button button-news button-arrow">Discover </span>
						</div>
					</div>
				</a>

				<a href="/our-businesses#labs">
					<div class="business-item business-item-6">
						<div class="title business-item_company">
							<img class="octopus-sp-title"
									 src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_L_Primary_289 RGB.png"/>
						</div>
						<div class="business-item_thumb"
								 style="background-image: url('/wp-content/themes/twentyseventeen/assets/images/labs.jpg')"
								 title="Energy"></div>
						<!--					<img src="/wp-content/themes/twentyseventeen/assets/images/labs.jpg" alt="Labs"/>-->
						<div>
						<span>
							Blending financial expertise with
							cutting-edge innovation to build smart,
							easy-to-use products and services.
						</span>
							<span class="custom-button button-news button-arrow">Discover</span>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="main-news main-news--line">
			<?php
			$args = array( 'post_type' => 'news', 'posts_per_page' => 10 );
			$loop = new WP_Query( $args );
			?>
			<h1 class="title">Latest news and insights</h1>
			<div class="mobile-title">News & insights</div>
			<div class="container">
				<div id="wrapper" class="wrapper">
					<?php
					while ( $loop->have_posts() ) : $loop->the_post();
						$author = get_post_custom_values( 'author' )[0];
						$photo  = get_post_custom_values( 'photo' )[0];
						?>
						<a href="<?php echo get_permalink(); ?>" class="item">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Octopus Group" />
							<div class="main">
								<div class="data">
									<div class="main-content">
										<div class="name"><?php echo $author; ?></div>
										<div class="date-item"><?php echo get_the_time( 'd.m.Y' ); ?></div>
									</div>
								</div>
								<span class="title"><?php the_title(); ?></span>
								<p class="content"><?php echo limit( wp_strip_all_tags( get_the_content() ), 90 ); ?></p>
								<span  class="read-more">
									Read More
									<span class="read-more-arrow"></span>
								</span>
							</div>
						</a>

						<?php
						/*printf( '<div class="item">
						    <img src="%s" alt="Octopus Group" />
							<div class="main">
						      <div class="data">
						      	<div class="main-content">
						        	<div class="name">%s</div>
						        	<div class="date-item">%s</div>
						      	</div>
						      </div>
							  <span class="title">%s</span>
							  <p class="content">%s</p>
								<a href="%s" class="read-more">
									Read More
									<span class="read-more-arrow"></span>
								</a>
							</div>
						  </div>', get_the_post_thumbnail_url(), $author, get_the_time( 'd.m.Y' ), get_the_title(), limit( wp_strip_all_tags( get_the_content() ), 90 ), get_permalink() );*/
//					var_dump(get_the_post_thumbnail_url());
					endwhile;
					?>
				</div>
			</div>
			<div class="scroller">
				<div id="draggable" class="scroll"></div>
			</div>
			<div class="view-all">
				<a href="/news">View all news</a>
			</div>
			<img class="arrows" src="/wp-content/themes/twentyseventeen/assets/images/arrows.png" alt="">
		</div>
	</div>
<?php get_footer();
<?php
/* Template Name: Career */
get_header();
?>
<?php //include 'other_menu.php'; ?>
	<div class="site-content-contain">
		<div id="content" class="site-content">
			<div class="career-main">
				<h1>Careers</h1>
				<div class="career-main-video">
					<!--        <iframe src=" https://www.youtube.com/embed/KUD0uxECbhs"></iframe>-->
					<iframe width="560" height="315"
									src="https://www.youtube.com/embed/KUD0uxECbhs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0"
									allowfullscreen></iframe>
				</div>
			</div>

			<section class="section">
				<div class="section_inner">
					<div class="career-about">
						<div class="left">
							<div class="title">
								We never stop looking for talented people who share our values.
							</div>
							<a target="_blank"
								 href="https://careers-octopus.icims.com/jobs/intro?hashed=-435715616&mobile=false&width=1360&height=500&bga=true&needsRedirect=false&jan1offset=120&jun1offset=180"
								 class="custom-button button-careers button-arrow">Open vacancies</a>
						</div>
						<div class="right">
							<p>Octopus is only as good as the people who work here. So we hire exceptional people, with hearts as well
								as brains. Then we ask them to come up with ways to make us even better. And, because it’s nice to say
								‘thank you’, we make them feel appreciated for doing it.
							</p>
							<p>If you’re committed to trying out new ideas and getting things done, and you genuinely care about
								people, then we want to hear from you.
							</p>
							<div class="title">
								Career progression, without the treadmill.
							</div>
							<p>
								Career success isn’t just about moving upwards. We nurture talent by helping people develop new skills
								and experiences. Which is why we offer support, training and advice to employees who want to go further.
								Your career with Octopus can be as traditional or unorthodox as you want it to be. If you love pushing
								yourself, there’s no limit to where we could take you.
							</p>
						</div>
					</div>
				</div>
			</section>
			<div class="career-main-video">
				<!--   <iframe src="https://www.youtube.com/embed/2AIEJfT3z6o"></iframe>-->
				<iframe width="560" height="315"
								src="https://www.youtube.com/embed/2AIEJfT3z6o?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0"
								allowfullscreen></iframe>
			</div>
			<section class="section">
				<div class="section_inner">
					<div class="advantage">
						<div class="left">
							<div class="title">
								Celebrating difference, embracing diversity
							</div>
							<p>
								Culture is a very important part of Octopus, and everyone has the opportunity to make a contribution and
								leave an imprint. Don't be afraid to speak up, to challenge the way things are or to suggest trying
								something new.
							</p>
						</div>
						<div class="right">
							<div class="title">&nbsp;</div>
							<p>
								As a company, we also understand the importance of diversity, and we have a team dedicated to promoting
								gender, ethnic, socio-economic, LGBT and disability equality, to make sure we back up our words with
								actions. We’re building a workplace environment where everyone can feel comfortable with who they are.
							</p>
						</div>
					</div>
					<a href="https://careers-octopus.icims.com/jobs/intro?hashed=-435715616&mobile=false&width=1360&height=500&bga=true&needsRedirect=false&jan1offset=120&jun1offset=180"
						 class="custom-button button-careers button-arrow" target="_blank">Latest vacancies</a>
				</div>
			</section>
<?php get_footer();
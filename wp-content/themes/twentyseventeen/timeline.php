<?php 
    /* Template Name: TimeLine */
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div class="line">&nbsp;</div>
<div id="content" class="site-content">
<span class="currency">Billion (£)</span>
<div class="message">
  <div class="first">
    <div class="wrapper">
      <div>
        <p>New Customers</p>
        <span class="customers">105,000</span>
      </div>
      <div>
        <p>Team size</p>
        <span class="team">150</span>
      </div>
      <div>
        <p>Funds under management</p>
        <span class="amount">£100M</span>
      </div>
    </div>
  </div>
  <div class="shadow"></div>
  <div class="second">
    <div class="carousel">
      <div>
        18 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        17 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        16 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        15 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        14 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        13 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        12 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        11 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        10 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        9 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        8 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        7 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        6 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        5 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        4 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        3 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        2 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
      <div>
        1 The group starts taking shape: Octopus Labs - initially known as ‘Skunkworks’ - is founded.
        Award-winning Dragonfly Property Finance is renamed Octopus Property.
        Octopus Energy is launched and almost instantly becomes the UK’s top-rated energy supplier for its customer service.
      </div>
    </div>
  </div>
  <div id="left" class="button button-left"></div>
  <div id="right" class="button button-right"></div>
  <div class="companies">
    <div class="title">Companies</div>
    <ul>
      <li>octopus<span>property</span></li>
      <li>octopus<span>labs</span></li>
      <li>octopus<span>healthcare</span></li>
    </ul>
  </div>
</div>
<canvas id="myChart" height="450" width="800"></canvas>

<?php get_footer();
<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
//echo get_post_type('news');
// ?search=&sort=newest&business=investments&category=insight&category=news
//'search' => string 'agagg' (length=5)
//          'sort' => string 'newest' (length=6)
//          'business' => string 'healthcare' (length=10)
//          'category' =>
//            array (size=2)
//              0 => string 'insight' (length=7)
//              1 => string 'news' (length=4)

$args                  = [];
$search_args           = [];
$insight_posts         = [];
$news_posts            = [];
$news_search_active    = false;
$insight_search_active = false;

if ( isset( $_GET ) && ! empty( $_GET ) ) {
	$query_string = $_SERVER['QUERY_STRING'];

	$query_chunks = explode( '&', $query_string );

	foreach ( $query_chunks as $chunk ) {
		$child_chunks = explode( '=', $chunk );

		if ( $child_chunks[0] == 'category' ) {
			$args[ $child_chunks[0] ][] = $child_chunks[1];
		} else {
			$args[ $child_chunks[0] ] = $child_chunks[1];
		}
	}

	if ( ! count( $args['category'] ) ) {
		$insight_search_active = true;
		$news_search_active    = true;
	}

	if ( count( $args['category'] ) > 0 ) {
		for ( $i = 0; $i < count( $args['category'] ); $i ++ ) {
			if ( $args['category'][ $i ] == 'insight' ) {
				$insight_search_active = true;
			}
			if ( $args['category'][ $i ] == 'news' ) {
				$news_search_active = true;
			}
		}
	}

	$meta_args = [];

	if ( $args['sort'] ) {
		$sort                   = $args['sort'];
		$search_args['orderby'] = 'post_date';

		if ( $args['sort'] == 'newest' ) {
			$search_args['order'] = 'DESC';
		}
		if ( $args['sort'] == 'oldest' ) {
			$search_args['order'] = 'ASC';
		}
	}

	if ( isset( $args['business'] ) && ! empty( $args['business'] ) ) {
		$business    = $args['business'];
		$meta_args[] = [
				'key'     => 'author',
				'value'   => $args['business'],
				'compare' => 'LIKE'
		];
	}

	if ( $args['search'] ) {
		$search_string    = $args['search'];
		$search_args['s'] = $args['search'];
	}


	/* --- NEWS POSTS QUERY --- */
	if ( $news_search_active ) {
		$search_args['post_type']  = 'news';
		$search_args['meta_query'] = $meta_args;
		$news_posts_q              = new WP_Query( $search_args );
		$news_posts                = $news_posts_q->posts;
	}

	/* --- INSIGHT POSTS QUERY --- */
	if ( $insight_search_active ) {
		$search_args['post_type']     = 'post';
		$search_args['category_name'] = 'insight';
		$insight_posts_q              = new WP_Query( $search_args );
		$insight_posts                = $insight_posts_q->posts;
	}

	$posts = array_merge( $news_posts, $insight_posts );

	/* Restore original Post Data */
	wp_reset_postdata();
}
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<!--	<div class="line">&nbsp;</div>-->
	<div id="content" class="giving site-content">
		<h1>News and insights</h1>
		<div class="main-news"
				 style=<?php echo wp_count_posts( 'news' )->publish > 2 ? "justify-content: space-between" : ""; ?>>
			<div class="news_grid">
				<div class="news_cell news_cell--form">
					<?php get_template_part( 'template-parts/news', 'form' ); ?>
				</div>


				<?php
				if ( count( $posts ) > 0 ) {
					foreach ( $posts as $post ) { ?>
						<div class="news_cell">
							<?php get_template_part( 'template-parts/news', 'item' ); ?>
						</div>
						<?php
					}
				} else {
					?>
					<div class="news_cell">
						<div class="no-results">
							<span>No news found</span>
							<a href="/news" class="button-controls custom-button">View all news</a>
						</div>
					</div>
				<?php }
				?>
			</div><!--news_grid-->

			<div class="pagination-wrapp">
				<?php the_posts_pagination(array(
						'show_all'     => false,
						'end_size'     => 1,
						'mid_size'     => 1,
						'prev_next'    => false,
				)); ?>
			</div>
		</div>
		<?php get_footer(); ?>

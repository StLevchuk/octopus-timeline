<?php

add_filter( 'wp_mail', 'my_wp_mail_filter' );

function my_wp_mail_filter( $args ) {

	$new_wp_mail = array(
			'to'          => $args['to'],
			'subject'     => $args['subject'],
			'message'     => $args['message'],
			'attachments' => $args['attachments'],
	);

	$new_wp_mail['headers'] = str_replace('X-WPCF7-Content-Type: text/html', '', $args['headers']);

	//do_action("wp_error_logger", $new_wp_mail);

	return $new_wp_mail;
}


add_action("wp_error_logger", "wp_error_logger_function", 10, 1);

//add_action("wp_mail", "wp_error_logger_function", 1, 1);

function wp_error_logger_function($err) {
	var_dump(json_encode( $err ));
}

//do_action("wp_error_logger", "test");
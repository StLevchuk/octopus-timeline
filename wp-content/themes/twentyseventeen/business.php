<?php 
    /* Template Name: Business */ 
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<div id="primary" class="content-area">
	<main id="main" class="business-main" role="main">
        <h1>Our businesses</h1>
        <div class="items">
            <div class="item" id="investments">
                <div>
                    <div class="title">
                        <img class="business-item__title" src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_I_Primary_289 RGB.png" />
                    </div>
                    <img src="/wp-content/themes/twentyseventeen/assets/images/Investments-business.jpg" alt="Investments"/>
                </div>
                <div>
                    <div class="title">
                        <span>Octopus</span>
                        <span>Investments</span>
                    </div>
                    <span>
                        We manage £6.7 billion* in assets for private investors and institutions.
                        And we’re changing the world of investments for the better, with simple, jargon-free products that do what they say they will. 
                        We currently manage £1.6 billion* on behalf of institutional investors,
                        and our fast-growing institutional investor base includes pension funds, asset managers, fund-of-funds and family offices.
                        <br>
                    </span>
                    <a href="http://octopusinvestments.com/"  target='_blank' class="custom-button button-news button-arrow" >Discover</a>
                </div>
            </div>
            <div class="item" id="healthcare">
                <div>
                    <div class="title">
                        <img class="business-item__title" src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_H_Primary_289 RGB.png" />
                    </div>
                    <img src="/wp-content/themes/twentyseventeen/assets/images/HealtCare-business.png" alt="Healthcare"/>
                </div>
                <div>
                    <div class="title" >
                        <span>Octopus</span>
                        <span>Healthcare</span>
                    </div>
                    <span>
                        We invest in GP surgeries, care homes, retirement housing, special education needs schools and private hospitals. 
                        As specialists in healthcare, we’re all about taking the long-term partnership approach, 
                        creating brilliant new environments to improve people’s health and wellbeing experience, for the better.
                    </span>
                    <a href="http://www.octopushealthcare.com/"  target='_blank' class="custom-button button-news button-arrow">Discover</a>
                </div>
            </div>
            <div class="item" id="ventures">
                <div>
                    <div class="title">
                        <img class="business-item__title" src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_V_Primary_289 RGB.png" />
                    </div>
                    <img src="/wp-content/themes/twentyseventeen/assets/images/Ventures-business.png" alt="Ventures"/>
                </div>
                <div>
                    <div class="title">
                        <span>Octopus</span>
                        <span>Ventures</span>
                    </div>
                    <span>
                        When it comes to venture capital, we’re the entrepreneur’s choice, dedicated to helping unusually talented teams scale fast. We know what it takes to turn good ideas into great businesses, and offer our portfolio companies bespoke coaching, consulting and mentoring. Based in London and New York, we’ve backed household names including Zoopla, Secret Escapes and graze.com.
                    </span>
                    <a href="http://octopusventures.com"  target='_blank' class="custom-button button-news button-arrow">Discover</a>
                </div>
            </div>
            <div class="item" id="property">
                <div>
                    <div class="title">
                        <img class="business-item__title" src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_P_Primary_289 RGB.png" />
                    </div>
                    <img src="/wp-content/themes/twentyseventeen/assets/images/Property-business.png" alt="Property"/>
                </div>
                <div>
                    <div class="title">
                        <span>Octopus</span>
                        <span>Property</span>
                    </div>
                    <span>
                        We provide market-leading customer service and innovative products to UK property investors. 
                        Known for straight-talking and a common sense approach to lending, we structure bespoke deals across the residential, 
                        commercial and development sectors. Since 2009, we’ve lent more than £2.3 billion* and won almost every industry award going.
                        <br>
                    </span>
                    <a href="http://www.octopusproperty.com/"   target='_blank' class="custom-button button-news button-arrow">Discover</a>
                </div>
            </div>
            <div class="item" id="energy">
                <div>
                    <div class="title">
                        <img class="business-item__title" src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_E_Primary_289 RGB.png" />
                    </div>
                    <img src="/wp-content/themes/twentyseventeen/assets/images/energy.jpg" alt="Energy"/>
                </div>
                <div>
                    <div class="title">
                        <span>Octopus</span>
                        <span>Energy</span>
                    </div>
                    <span>
                        We want to make energy greener, smarter and more affordable for everyone. Not only are we one of the biggest generators of renewable energy in the UK, we’re also one of the top-rated energy suppliers. With tariffs that are clear and always good value, we’re helping households and businesses to save money and carbon without ever feeling locked-in.
                    </span>
                    <a href="https://octopus.energy/"  target='_blank' class="custom-button button-news button-arrow">Discover</a>
                </div>
            </div>
            <div class="item" id="labs">
                <div>
                    <div class="title">
                        <img class="business-item__title" src="/wp-content/themes/twentyseventeen/assets/images/home/Octopus_L_Primary_289 RGB.png" />
                    </div>
                    <img src="/wp-content/themes/twentyseventeen/assets/images/labs.jpg" alt="Labs"/>
                </div>
                <div>
                    <div class="title">
                        <span>Octopus</span>
                        <span>Labs</span>
                    </div>
                    <span>
                       We’re developing easy-to-use products and services to help people manage their money. 
                       We combine established financial expertise with cutting-edge technology to build smart, simple and fintech products for the mass market.
                       We also have an accelerator program to help start-ups bring their solutions to the financial adviser market.
                    </span>
                    <a href="http://www.octopuslabs.com/"  target='_blank' class="custom-button button-news button-arrow">Discover</a>
                </div>                            
            </div>
					<div style="padding: 20px; display: block; text-align: left; width: 100%;">
						<i>* Source: Octopus at 31 March 2017</i>
					</div>
	    </main>
    </div>    
<?php get_footer();

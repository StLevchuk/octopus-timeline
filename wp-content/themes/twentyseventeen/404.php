<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<div class="error">
	<div class="number-error">
		404
	</div>
	<div class="text-error">
		Page not found
	</div>
	<a href="/" class="custom-button button-back-home button-arrow">Back to Homepage</a>	
</div>

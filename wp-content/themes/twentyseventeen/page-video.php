<?php
/**
 *Template Name: Video page
 *
 */

get_header(); ?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
		<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'video' );

			endwhile; // End of the loop.
			?>

	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();

<header class="head-block other-menu">
  <div class="logo">
    <div class="title">
      <a href="/">
        <img src="./assets/images/octopus_logo.png" alt="">
      </a>
    </div>
  </div>
  <ul id="menu" class="menu">
    <li>
      <a href="/a-brighter-way">A brighter way</a>
      <div class="item-border"></div>
    </li>
    <li class="container">
      <a class="menu-item">Discover Octopus</a>
      <div class="item-border"></div>
        <div class="sub-menu hide">
        <div class="triangle"></div>
          <ul class="menu-list">
            <li><a href="/discover-octopus/about-us">About us</a></li>
            <li class="center"><a href="/discover-octopus/leadership-team">Our leadership team</a></li>
            <li><a href="/discover-octopus/octopus-giving">Octopus Giving</a></li>
          </ul>
      </div>
    </li>
    <li>
      <a href="/our-businesses">Our businesses</a>
      <div class="item-border"></div>
    </li>
    <li>
      <a href="/news">News and insights</a>
      <div class="item-border"></div>
    </li>
    <li>
      <a href="/careers">Careers</a>
      <div class="item-border"></div>
    </li>
    <li>
      <a href="/contact-us">Contact us</a>
      <div class="item-border"></div>
    </li>
  </ul>
</header>


<div class="wrapper-mobile-menu">
  <div class="mobile-header">
    <div class= "mobile-logo"></div>
    <div id="mobile-icon-menu" class="mobile-icon-menu"></div>
  </div>
  <div class="mobile-menu">
    <div id="close" class="close"></div>
    <div class="title">
      <img src="./assets/images/logo_white.svg" alt="">
    </div>
    <ul class="menu-list">
      <li><a href="/a-brighter-way">A brighter way</a></li>
      <li><a href="/discover-octopus/about-us">About us</a></li>
      <li><a href="/discover-octopus/leadership-team">Our leadership team</a></li>
      <li><a href="/discover-octopus/octopus-giving">Octopus giving</a></li>
      <li><a href="/our-businesses">Our businesses</a></li>
      <li><a href="/news">News and insights</a></li>
      <li><a href="/careers">Careers</a></li>
      <li><a href="/contact-us">Contact Us</a></li>
    </ul>
  </div>
</div>
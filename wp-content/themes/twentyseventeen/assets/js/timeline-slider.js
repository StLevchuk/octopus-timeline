function setValuesStatistics(number) {
  $('.customers').html(statistic[number].customers);
  $('.team').html(statistic[number].team);
  $('.amount').html(statistic[number].amount);
}

function moveText(number, count) {
  var itemWidth = $('.carousel > div').first().outerWidth();
  $('.carousel').css('margin-left', '-'+ itemWidth * (count - number) +'px');
  setValuesStatistics(number);
}

(function () {
  $(document).ready(function() {
    var carouselWidth = $('.carousel').width();
    var itemWidth = $('.carousel > div').first().outerWidth();
    var amoutElemens = $('.carousel > div').size() - 1;
    $('.button').on('click', $.throttle(1000, true, function(e) {
      var marginLeft = parseInt($('.carousel').css('margin-left'), 10);
      if (e.target.id === 'left' && carouselWidth - itemWidth >= Math.abs(marginLeft - itemWidth)) {
        setValuesStatistics(amoutElemens - Math.abs(marginLeft - itemWidth) / itemWidth);
        $('.carousel').css('margin-left', '-=' + itemWidth + 'px');
      } else if (e.target.id === 'right' && (marginLeft + itemWidth) <= 0) {
        setValuesStatistics(amoutElemens - Math.abs(marginLeft + itemWidth) / itemWidth);
        $('.carousel').css('margin-left', '+=' + itemWidth + 'px');
      }
    })
    );
   }); 
})();
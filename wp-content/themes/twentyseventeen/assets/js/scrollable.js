(function () {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) return false;
    var wrapper = $('.wrapper');
    var scrollerWidth = $('.scroller').width();
    var containerWidth = $('.site-content').width();
    var wrapperWith = wrapper.width();
    if (containerWidth < wrapperWith) {
      var scollWidth = parseInt(containerWidth * 100 / wrapperWith);
       $("#draggable").draggable({ axis: "x",
        containment: "parent",
        drag: function(event, ui) {
          var offset = ui.position.left;
          var inPercent = parseInt(offset * 100 / scrollerWidth);
          wrapper.css('marginLeft', -parseInt(wrapperWith * inPercent / 100));
        },
      }).width(parseInt(scrollerWidth * scollWidth / 100));
    }
  });
})();

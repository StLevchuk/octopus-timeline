(function() {
  $(window).load(function() {
    var canvas = document.getElementById('myChart');
    var data = {
    labels: [ "2000", "2001", "2002", "2003",
      "2004", "2005", "2006",
      "2007", "2008", "2009",
      "2010", "2011", "2012",
      "2013", "2014", "2015", 
      "2016", "2017"
    ],
    datasets: [
      {
        label: false,
        fill: true,
        backgroundColor: 'rgba(117, 182, 247, 0.53)',
        borderColor: 'rgba(117, 182, 247,0.53)',
        borderCapStyle: 'round',
        borderWidth: 0.1,
        pointBorderColor: "rgba(255,255,255,1)",
        pointBackgroundColor: "rgba(255,1,115,1)",
        pointBorderWidth: 2,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(255,1,115,1)",
        pointHoverBorderColor: "rgba(255,1,115,0.23)",
        pointHoverBorderWidth: 7,
        pointRadius: 5,
        pointHitRadius: 10,
        data: [0, 0.1, 0.2, 0.3, 0.6, 0.7, 1, 1.12, 2, 3, 3, 4, 4, 4, 5, 5, 6, 6.1, 6.5],
      }
    ]
};

var option = {
  tooltips: {
    displayColors: false,
    backgroundColor: "rgba(255,1,115,1)",
    xPadding: 20,
    titleFontFamily: 'Brown Regular',
    titleMarginBottom: 0,
    titleFontSize: 16,
    titleSpacing: 0,
    bodyFontSize: 16,
    callbacks: {
      title: function(tooltipItem, data) {
        return "£ " + tooltipItem[0].yLabel + " Billion";
      },
      label: function(tooltipItem, data) {
        return false;
      } 
    }
  },
  legend: {
    display: false
  },
  scales: {
    xAxes: [{
      gridLines: {
        display: false
      }
    }], 
    yAxes: [{
      ticks: {
        min: 0,
        max: 7,
        stepSize: 1
    }}]
  },
  animation: {
    duration: 0
  }
};

var myLineChart = Chart.Line(canvas,{ data: data, options: option });

$('#myChart').on('click', function(elm) {
  var elements = myLineChart.getElementsAtEvent(elm);
  var elm = myLineChart.getElementAtEvent(elm);
  if (elm[0]) {
    var ctx = canvas.getContext('2d');
    var cornerRadius = 20;
    var height = 1;
    var width = 40;
    var offsetLeft = 21;
    var lineToY = elm[0]._chart.height - 22;
    var offsetY = elm[0]._chart.height - 12;
    var offsetYText = elm[0]._chart.height - 21;
    ctx.beginPath();
    ctx.arc(elm[0]._model.x, elm[0]._model.y, 10, 0, 180);
    ctx.lineWidth = 10;
    ctx.strokeStyle = "rgba(255,1,115,.2)";
    ctx.stroke();
    ctx.beginPath();
    ctx.strokeStyle = "#FF0173";
    ctx.stroke();
    ctx.textAlign = 'center';
    ctx.fillStyle = "#FF0173";
    ctx.fillRect(elm[0]._model.x - offsetLeft, offsetY, width, height);
    ctx.lineJoin = "round";
    ctx.lineWidth = cornerRadius;
    ctx.strokeRect(elm[0]._model.x - offsetLeft, offsetY, width, height);
    ctx.lineWidth = 1;
    ctx.moveTo(elm[0]._model.x, elm[0]._model.y);
    ctx.lineTo(elm[0]._model.x, lineToY);
    ctx.fillStyle = "#fff";
    ctx.fillText(elm[0]._xScale.ticks[elm[0]._index], elm[0]._model.x, offsetYText);
    ctx.stroke();
    moveText(elm[0]._index, elm[0]._chart.config.data.labels.length-1);
  }
})
  });
})();
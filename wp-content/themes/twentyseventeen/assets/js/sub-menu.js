function showSubMenu() {
  $('.menu-item').on('click', function(event) {
    event.preventDefault();
    $('.sub-menu').toggle();
  });
};

(function() {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    if (!md.mobile()) showSubMenu();
  })
})();
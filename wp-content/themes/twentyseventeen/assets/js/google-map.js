function drawPath(finishPosition) {
	console.log("click");
	if (!navigator.geolocation) {
		console.log("click false");
		alert("Geolocation is not supported by this browser.");
		return;
	}
	var basePath = ("https://www.google.com/maps/dir/%home-lat,%home-lng/%finish/").replace("%finish", finishPosition);
		var test = navigator.geolocation.getCurrentPosition(function (position) {
			var currentPath = basePath.replace("%home-lat", position.coords.latitude).replace("%home-lng", position.coords.longitude);
		});
		// console.log(test);
		if(!test) {
			var currentPath = "http://maps.google.com/?q="+ finishPosition;
			console.log(currentPath);
			window.open(currentPath, '_blank');
		}


}

google.maps.event.addDomListener(window, "load", function () {
	var mapContainer = document.getElementById('contacts-map');
	if (!mapContainer) return;
	var Investments = {lat: 51.5173090, lng: -0.1085271};
	var Energy = {lat: 51.5138264, lng: -0.1355063};
	var Center = {lat: 51.52978531765366, lng: -0.12256622314453125};
	var Zoom = {
		desktop: 14,
		mobile: 13
	};
	var MobileWidth = 575;
	var currentLocation = null;

	var GeneralLayout = [
		"<div class='marker-content'>",
		"<h2>%s0</h2>",
		"<p>%s1</p>",
		"<p>London</p>",
		"<p>%s2</p>",
		"<a onclick='drawPath(\"%s3\")' id='direction-link'>Get Directions</a>",
		"</div>"
	].join(' ');

	var infoInvestments = new google.maps.InfoWindow({
		content: GeneralLayout.replace("%s0", "Octopus Group").replace('%s1', "33 Holborn").replace('%s2', "EC1N 2HT").replace('%s3', "33 Holborn, London EC1N 2HT,  London, UK")
	});

	var infoEnergy = new google.maps.InfoWindow({
		content: GeneralLayout.replace("%s0", "Octopus Energy").replace('%s1', "20-24 Broadwick St").replace('%s2', "W1F 8HT").replace('%s3', "20-24 Broadwick St, Soho, London W1F 8HT, London, UK")
	});

	var isDraggable = $(document).width() > 768 ? true : false; // If document (your website) is wider than 768px, isDraggable = true, else isDraggable = false

	var map = new google.maps.Map(mapContainer, {
		zoom: ((document.body.clientWidth > MobileWidth) ? Zoom.desktop : Zoom.mobile),
		center: Center,
		draggable: isDraggable,
		scrollwheel: false // Prevent users to start zooming the map when scrolling down the page

	});

	var markerInvestments = new google.maps.Marker({
		position: Investments,
		map: map
	});

	var markerEnergy = new google.maps.Marker({
		position: Energy,
		map: map
	});

	markerInvestments.addListener('click', function () {
		infoInvestments.open(map, markerInvestments);
	});

	markerEnergy.addListener('click', function () {
		infoEnergy.open(map, markerEnergy);
	});

	infoInvestments.open(map, markerInvestments);
	infoEnergy.open(map, markerEnergy);

	window.addEventListener("resize", function (e) {
		var width = document.body.clientWidth;
		var height = document.body.clientHeight;
		if (width < MobileWidth) map.setZoom(Zoom.mobile);
		else map.setZoom(Zoom.desktop);
	});

	google.maps.event.addListener(map, 'click', function (event) {
		var position = event.latLng;
		console.warn(position.lat(), position.lng());
	});
});
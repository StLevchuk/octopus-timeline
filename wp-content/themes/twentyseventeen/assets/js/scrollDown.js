(function() {
  $(document).ready(function() {
    var sizeScreen = screenHeigth();
    var scrollOffset = scrollTop();
    setTimeout(function() {
      var headerHeight = $('header').height();
      var sumScrollAndHeight = sizeScreen + scrollOffset;
      if (sumScrollAndHeight + 100 >= headerHeight) {
        $('.scroll-down').css('bottom', '8%');
      } else {
        $('.scroll-down').css('top', Math.ceil((sumScrollAndHeight) * 100 / headerHeight) + '%');  
      }
      });

      $('.scroll-down').on('click', function() {
        $('html, body').animate({
          scrollTop: $('.site-content-contain').offset().top - 100
      }, 1000);
      });
      
    }, 1000);
    
})();


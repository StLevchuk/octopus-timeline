function slider() {
  var offset = 7;
  var widthContainer = ($('.container').width() - $('.item').first().width()) / 2;
  var wrapper = $('#wrapper');
  var itemOfWrapper = $('.item').first().width();
  var news = document.getElementById('wrapper');
  var newsHm = new Hammer(news);
  newsHm.on("panleft panright", function(ev) {
    if (ev.type === 'panleft')  {
      var between = parseInt(wrapper.css('margin-left')) + wrapper.width();
      if (between <= itemOfWrapper + widthContainer) return;
      wrapper.css({ "margin-left": '+=-'+ offset });
    } else if (parseInt(wrapper.css('margin-left')) <= 0) {
      wrapper.css({ "margin-left": '+=' + offset });
    } 
  });
}

(function() {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    try {
			if (md.mobile()) slider();
		} catch (e) {
      console.log(e);
    }

  });
})();

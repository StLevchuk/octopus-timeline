$(function () {

	// var $playerWrapp = $(".js-player-wrapp");
	// var player = videojs('video-payer', {controls: false, autoplay: false, preload: 'auto'});
	var $player = $('#video-payer');
	var player = document.getElementById('video-payer');
	var playBtn = $(".js-play-btn");
	// var isPlaying = false;

	// player.on("loadedmetadata", function () {
	// 	alert("test");
	// });

	// player.on(['waiting', 'pause'], function () {
	// 	isPlaying = false;
	// 	playBtn.show();
	// });
	//
	// player.on('playing', function () {
	// 	isPlaying = true;
	// 	playBtn.hide();
	// });

	function playToggle() {
		var isPlaying = !player.paused;
		if (isPlaying) {
			playBtn.show();
			player.pause();
		} else {
			playBtn.hide();
			player.play();
		}
	}

	var isMobile = new MobileDetect(window.navigator.userAgent).mobile();
	//console.log(isMobile);

	var handleClick = function () {

		if (isMobile) {
			// player.addEventListener("fullscreenchange", function (event) {
			// 	var isFullscreen = player.isFullscreen();
			// 	console.log(isFullscreen);
			// 	if (!isFullscreen) {
			// 		player.pause();
			// 		//player.hide();
			// 	}
			// });

			//player.requestFullscreen();
			//player.play();
			playToggle();
		} else {
			playToggle();
		}

	};


	$player.on("play", function() {

		dataLayer.push({
			event: "playQuestionPageVideo"
		});
		//console.log(window.dataLayer);
	});

	$player.on("click", function (e) {
		e.preventDefault();
		e.stopPropagation();
		handleClick();

		// console.log("click");
	});

	// $('body').on('click touchend', ".js-player-wrapp", function (event) {
	// 	console.log("touch");
	// 	var documentClick = (event.type == "click") ? true : false;
	//
	// 	if (documentClick) {
	// 		console.log("touch+click");
	// 	}
	// });
	// $("#test").on("click", function () {
	// 	this.play();
	// });
});
function showMenuMobile() {
  var menuIcon = document.getElementById('mobile-icon-menu');
  var menu = new Hammer(menuIcon);
  menu.on("tap", function(ev) {
    $('.mobile-menu').addClass('show');
    $('.wrapper-mobile-menu').addClass('show-menu');
    $('.wrapper-mobile-menu').css('height',  screen.height);
  });

  var closeMenu = document.getElementById('close');
  var menuClose = new Hammer(closeMenu);
  menuClose.on("tap", function(ev) {
    $('.mobile-menu').removeClass('show');
    $('.wrapper-mobile-menu').removeClass('show-menu');
    if ($('#page').hasClass('no_home')) {
      $('.wrapper-mobile-menu').css('height', 69);
    } else {
      $('.wrapper-mobile-menu').css('height', 0);
    }
  });
}

function showMenuDesktop() {
  var menuIcon = document.getElementById('mobile-icon-menu');
  var closeMenu = document.getElementById('close');
  
  menuIcon.addEventListener('click', function() {
     $('.mobile-menu').addClass('show'); 
  });

  closeMenu.addEventListener('click', function() {
     $('.mobile-menu').removeClass('show'); 
  });
}

(function() {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) { 
      showMenuMobile();
    } else {
      showMenuDesktop();
    }
  });
})();
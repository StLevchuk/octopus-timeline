function scrolling() {
  var controller = new ScrollMagic.Controller();
  var scene1 = new ScrollMagic.Scene({triggerElement: "#trigger-one", triggerHook: 0.8 })
    .setClassToggle(".business-item-1, .business-item-2, .business-item-3", "showElement")
    .addTo(controller);
  var scene2 = new ScrollMagic.Scene({triggerElement: "#trigger-two", triggerHook: 0.8 })
    .setClassToggle(".business-item-4, .business-item-5, .business-item-6", "showElement")
    .addTo(controller);
}

(function() {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    if (!md.mobile()) scrolling();
  });
})()

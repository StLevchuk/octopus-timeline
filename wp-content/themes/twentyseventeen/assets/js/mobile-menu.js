(function() {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) {
      Hammer.on(window, "scroll", function(ev) {
        if (scrollTop() > 69) {
          $('.wrapper-mobile-menu').addClass('fixed');
        } else {
          $('.wrapper-mobile-menu').removeClass('fixed');

        }
      });
    } 
  });
})();

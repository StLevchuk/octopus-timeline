(function() {
  $(document).ready(function() {
    var md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) return false;
    $(window).scroll(function(e) {
      if ($(window).scrollTop() > 100) {
        $('.js-fixed-nav, .wrapper-mobile-menu').addClass('fixed');
        //$('.main-menu').addClass('hide');
      } else {
        $('.js-fixed-nav, .wrapper-mobile-menu').removeClass('fixed');
        //$('.main-menu').removeClass('hide');
      }
    })
  });
})()
function VideoHandler() {
  var self = this;
  this.mobileDetect = new MobileDetect(window.navigator.userAgent);
  this.videoInstances = new Array(0);

  this.handleClick = function() {
    var isMobile = self.mobileDetect.mobile();

    if(isMobile) {
        self.videoInstances[0].on('fullscreenchange', function(event) {
            var isFullscreen = self.videoInstances[0].isFullscreen();

            if(!isFullscreen) {
                self.videoInstances[0].pause();
                self.videoInstances[0].hide();
            }
        });

        self.videoInstances[0].requestFullscreen();
        self.videoInstances[0].play();
    } else {
        self.videoInstances[1].play();

        $('#video').addClass('hide');
        $('#video_main').addClass('show');
        $('#mask').removeClass('mask');
        $('.main-block').hide();
        $('.plyr--ready').addClass('background');
    }
  };

  this.togglePlay = function(instance) {
    var toggle = !instance.paused();

    if(toggle) {
        instance.pause();
    } else {
        instance.play();
    }
  }

  this.reloadDesktop = function() {
    self.videoInstances[0].pause();
    $('.main-block').show();
    $('#video').removeClass('hide');
    $('#video_main').removeClass('show');
    $('#mask').addClass('mask');
  }

};

function get(selector) {
    return document.querySelector(selector);
};

function on(element, type, callback) {
    if (!(element instanceof HTMLElement)) {
        element = get(element);
    }
    element.addEventListener(type, callback, false);
};

function initVideoHandle() {
    var videoHandler = new VideoHandler();
    var isMobile = videoHandler.mobileDetect.mobile();
    $('body').on('click', '.play-video', videoHandler.handleClick);

    if (isMobile) {
        videoHandler.videoInstances = [ 
            videojs('video', { controls: false, autoplay: false, preload: 'auto'})
        ];
    } else {
        videoHandler.videoInstances = [
            videojs('video', { controls: false, autoplay: true, preload: 'auto'}),
            videojs('video_main', { controls: false, autoplay: false, preload: 'auto'})
        ];

        videoHandler.videoInstances[1].on('click', function() {
            videoHandler.togglePlay(videoHandler.videoInstances[1]);
        });

        videoHandler.videoInstances[1].on('ended', videoHandler.reloadDesktop);
    }
};

$(document).ready(initVideoHandle);
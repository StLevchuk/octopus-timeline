<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
</div><!-- #content -->
   <footer id="colophon" class="site-footer" role="contentinfo">
	 <div class="main-bottom-navigation">
		<div class="logo-footer"><a href="/"><img src="./assets/images/footer_logo.svg"></a></div>
		<div class="navigation-description">
			<div>
				<div class="title">Octopus Group</div>
				<ul>
					<li>
						<a href="/a-brighter-way">
							A brighter way
						</a>
					</li>
					<li>
						<a href="/discover-octopus/about-us">
							About us
						</a>
					</li>
					<li>
						<a href="/discover-octopus/leadership-team">
							Our leadership team
						</a>
					</li>
					<li>
						<a href="/discover-octopus/octopus-giving"">
							Octopus Giving
						</a>
					</li>
					<li>
						<a href="/our-businesses">
							Our businesses
						</a>
					</li>
					<li>
						<a href="/news">
							News and insights
						</a>
					</li>
				</ul>
			</div>
			<div>
				<div class="title">&nbsp;</div>
				<ul>
					<li>
						<a href="/career">
							Careers
						</a>
					</li>
					<li>
						<a href="/contact-us">
							Contact us
						</a>
					</li>
					<li>
						<a href="/terms-and-conditions">
							Terms and conditions
						</a>
					</li>
					<li>
						<a href="/privacy-policy">
							Privacy policy
						</a>
					</li>
					<li>
						<a href="/modern-slavery-act">
							Modern Slavery Act
						</a>	
					</li>
				</ul>
			</div>
			<div></div>
		</div>
	</div>
				<div class="main-footer">
					<span>© 2017 Octopus Capital Ltd</span>
					<span>All rights reserved</span>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>

<?php 
    /* Template Name: Contact Us */ 
    get_header(); 
?>

<?php
//include 'other_menu.php';

?>

<?php
//use google\appengine\api\mail\Message;
//
//$name=$email=$query="";
//
//if($_SERVER["REQUEST_METHOD"]=="POST"){
//
//    $name = $_POST["name"];
//    $email = $_POST["email"];
//    $query = $_POST["query"];
//
//    require_once 'google/appengine/api/mail/Message.php';
//    $mail_options = [
//        "sender" => $email,
//        "to" => "george@lanars.com",
//        "subject" => "Subject",
//        "textBody" => $query,
//    ];
//
//    try {
//        $message = new Message($mail_options);
//        $message->send();
//    } catch (InvalidArgumentException $e) {
//        echo "not send";
//    }
//}
//?>


<div class="site-content-contain">
<div id="content" class="site-content">  
<div class="contact-us">
  <h1>Contact us</h1>
  <div class="text">
    <div>
      Fill out the form to tell us how we can help. We’ll get back to you within 24 hours. 
    </div>
    <div>
      Or if you prefer an old-fashioned conversation, give us a call using the numbers below.
    </div>
  </div>
  <div class="container">
    <div class="left">
      <div class="title">
        Call us
      </div>
      <ul>
        <li>
          <div class="name">
            Octopus Investments 
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)800 316 2295
          </div>
        </li>
        <li>
          <div class="name">
            Octopus Healthcare  
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)203 142 4087
          </div>
        </li>
        <li>
          <div class="name">
            Octopus Ventures
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)20 7776 7968
          </div>
        </li>
        <li>
          <div class="name">
            Octopus Property
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)800 294 6850
          </div>
        </li>
        <li>
          <div class="name">
            Octopus Energy
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)333 344 2268 <span>or</span> +44 (0)808 164 1088
          </div>
        </li>
        <li>
          <div class="name">
            Octopus Labs
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)800 294 6848
          </div>
        </li>
      </ul>
      <div class="title">
        Press enquiries
      </div>
      <ul>
        <li>
          <div class="name">
            Call Georgina Turner
          </div>
          <div class="phone" x-ms-format-detection="none">
            +44 (0)20 7776 7968
          </div>
        </li>
        <li>
          <div class="name">
            Email&nbsp;<a class="email" href="mailto:press@octopusgroup.com">press@octopusgroup.com</a>
          </div>
        </li>
      </ul>
    </div>
    <div class="right">
			<div class="cf7-wrapper">
				<?php echo do_shortcode('[contact-form-7 id="176" title="Contact form"]'); ?>
			</div><!--search cf7-wrapper-->
    </div> 
  </div>
  <div id="contacts-map"></div>
</div>

<?php get_footer(); ?>
<?php 
    /* Template Name: Policy */ 
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<h1>PRIVACY POLICY</h1>
<div class="main policy">
  <p>Last updated on: 14 June 2017</p>
  <div class="title">ABOUT OCTOPUSGROUP.COM</div>  
  <p>This Privacy Policy sets out how we collect, use and protect your personal information. 'Personal information' is information which identifies you or another person, or which is capable of doing so.</p>
  <p>
    Octopusgroup.com (the “<b>Website</b>”) is owned by Octopus Capital Limited 
    ("<b>Octopusgroup.com</b>", "<b>we</b>" or "<b>us</b>"), 
    a company registered in England and Wales under company number 03981143 whose registered office is at 6th Floor, 33 Holborn, London, EC1N 2HT. 
  </p>
  <p>
    The data controller (as defined under European Data Protection Legislation) for Octopusgroup.com is Octopus Capital Limited.
    We are registered as a data controller with the Information Commissioner's Office and our registration number is 2A171166.
  </p>
  <p>
    <b>Please take the time to read this Privacy Policy, which is part of our Terms and Conditions.</b> 
    If you have any questions about this Privacy Policy or our use of your information you can contact us at digital@octopusgroup.com or +44 800 316 2295. 
    You also have the right, however, to make a complaint to the relevant Supervisory 
    Authority about our use of your information (in most cases under this Privacy Policy the relevant
    Supervisory Authority will be the Information Commissioner’s Office).
  </p>  
  <div class="title">ABOUT THIS PRIVACY POLICY</div>  
  <p>
    We take your privacy seriously and are committed to maintaining the privacy and security of information you provide to us, 
    and the choices you have regarding our collection and use of your information. 
    This Privacy Policy applies to information we collect about you through this Website and when you otherwise communicate with us. 
    By visiting our Website you are accepting and consenting to the practices described in this Privacy Policy. 
  </p>
  <p>
    We process your personal data on the basis that you have provided your consent for us to do so for the purposes set out below when you submitted your personal data to us. 
    You may withdraw your consent to this processing at any time by contacting us at digital@octopusgroup.com or +44 800 316 2295. 
    If you do withdraw your consent, we may still be able to process some of the data that you have provided to us on other grounds. 
  </p>
  <p>
    We have a philosophy here about the way we use your consent to be contacted by us (if you give us that consent). 
    If you hear from us, we will always aim to be respectful, relevant and appropriate. 
    If at any time you don't feel we've lived up to our philosophy, please let us know straight away by getting in touch with us.
  </p>  
  <p>
    This Privacy Policy may change from time to time and, if it does, the up-to-date version will always be available on our Website. 
    Please note that by continuing to use our Website you are agreeing to any updated versions.
  </p>  
  <div class="title">INFORMATION WE MAY COLLECT ABOUT YOU </div>  
  <p>We may collect and process the following data:</p> 
  <p>
    <b>Information you give to us. </b>  
    You may give us information about you by filling in forms (e.g. on our ‘Contact Us’ page), 
    by contacting us or by submitting a job application on our Website. The information you give to us may include: 
    <ul>
      <li>your name and title; </li>
      <li>contact information, including telephone number and email address;</li>
      <li>information such as postcodes, and your preferences and interests; and</li>
      <li>employment details, e.g. date of birth, employment history, qualifications</li>
    </ul>  
  </p> 
  <p>
    <b>Information we collect about you.</b>
     Each time you visit our Website, we may automatically collect the following information: 
     <ul>
      <li>web usage information (e.g. IP address), your login information, browser type and version, time zone setting, operating system and platform; and </li>
      <li>information about your visit, including the full Uniform Resource Locators (URLs) clickstream to, through and from Octopusgroup.com (including date and time); time on page, page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks and mouse-overs).</li>
    </ul>  
  </p>
  <p>
    <b>Information we receive from other sources.</b>
    <br> 
    We may also receive information about you if you use any of the other websites we operate (including, for example, Octopus Investments, Octopus Healthcare) or the other services we provide. For a full list of the other websites we operate please visit our businesses section of the Website
  </p>  
   <div class="title">COOKIE POLICY</div>  
   <p>
     Our Website uses cookies to distinguish you from other users and to improve your experience on our Website and to recommend content that may be of interest to you. When you visit the Website we will ask you whether you agree to our use of cookies.  You can indicate your acceptance of our use of cookies by clicking “OK” in the appropriate place on the banner that appears on the Website. However, please note that if you do not click “OK” but continue to browse the Website, you will be deemed to have accepted our use of cookies in accordance with this Privacy Policy. If you do not agree to our use of cookies, then you may continue to use the Website but your browsing experience may be affected. You can find out more below about what cookies are, the cookies we use and how to switch off cookies.
   </p>
   <p>
     <b>What are cookies?</b>
   </p>  
   <p>
     A cookie is a text file containing small amounts of information which is downloaded to your device when you access a website. The text file is then sent back to our server each time your browser requests a page from the server. This enables us to operate the Website more effectively and load the Website in such a way as to reflect your personal preferences based on your previous browsing on the Website, as well as keywords we may be able to gather from URLs of webpages from which you accessed the Website. 
   </p>

   <p>
     <b>What cookies do we use?</b>
     <br>
     We use the following cookies: 
     <ul>
       <li>
        <b>Analytical and performance cookies. </b>
        We may use analytics service providers for website traffic analysis and reporting. Analytics service providers generate statistical and other information about the use of the Website by using cookies. They allow us to recognise and count the number of visitors and to see how often visitors return to the Website, how long they stay and how visitors move around our Website when they are using it. This helps us to improve the way our Website works, for example, by ensuring that users are finding what they are looking for easily. The information generated relating to the Website may be used to create reports about the use of the Website and the analytics service provider will also store this information.
       </li>
       <li>
         <b>Advertising and targeting cookies. </b>
         These cookies record your visit to our Website, the pages you have visited and the links you have followed to other websites. We will use this information to try and make our Website and other websites that you visit more relevant to your interests. We may also share this information with others, such as other Octopus group companies and carefully selected third parties for this purpose.
       </li> 
       <li>
        <b>Functionality cookies </b>
         We may use functional cookies that allow us to remember choices you have made in relation to the Website so as to provide a more personalised experience and enhanced user experience by delivering content specific to your interests.
       </li>   
       <li>
         <b>Strictly necessary cookies. </b>
       </li>  
     </ul>
   </p>  
   <p> <b> Third party cookies</b></p>
   <p>
     We try and make it easy to share content and to see what content is popular on those networks. We add buttons to allow people to easily share to those networks. When we include these social 'plugins', it gives those sites the flexibility to use cookies. They can't read any cookies we set from our Website, and we can't read any cookies they set, but it lets them do the same kind of traffic measuring that we do on the rest of the Website, and it also lets them know whether you're logged in. Other websites and services (including, for example, advertising networks, providers of external services like web traffic analysis services and content recommendation engines) may also use cookies, over which we have no control. These cookies are likely to be analytical/performance cookies or targeting cookies. 
   </p>
   <p> <b>How do I turn cookies off?</b></p>
   <p>
    If you would like to opt out of cookies, you can change the settings on your internet browser to reject cookies. For more information please consult the “Help” section of your browser or visit www.aboutcookies.org or www.allaboutcookies.org. Please note that if you do set your browser to reject cookies, you may not be able to use all of the features of our Website. 
   </p>
    <div class="title">HOW WE WILL USE TOUR INFORMATION</div>
   <p>
     We may use your information for the following purposes:
     <ul>
       <li>to respond to any query that you may submit to us;</li>
       <li>to ensure that our Website’s content is presented in the most effective manner for you and your computer;</li>
       <li>to customise the Website according to your interests;</li>
       <li>to administer the Website and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey responses; </li>
       <li>to allow you to participate in interactive features of our service when you choose to do so;</li>
       <li>as part of our efforts to keep the Website safe and secure;</li>
       <li>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;</li>
       <li>to comply with the law;</li>
       <li>as we feel is necessary to prevent illegal activity or to protect our interests.</li>
     </ul>
   </p>
    <div class="title">DISCLOSURE OF YOUR INFORMATION</div>
    <p>We may share your personal information with any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006. </p>
    <p> When we share your information with third parties they will process your information as either a data controller or as our data processor and this will depend on the purposes of our sharing your personal data. We will only share your personal data in compliance with the applicable data protection legislation. 
    </p>
    <p>
        We may disclose your information to third parties when: 
        <ul>
          <li>you specifically request this, such as when you submit information to enquire about jobs or submit a job application through the Website;</li>
          <li>other Octopus companies products and services may interest you;</li>
          <li>in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;</li>
          <li>if the Website or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets; or</li>
          <li>if we are under a duty to disclose or share your personal data in order to comply with any legal obligation or to protect the rights, property or safety of the Website, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction. </li>
        </ul>
    </p>
    <p>
        The third parties include:
        <ul>
          <li>business partners, clients, suppliers and sub-contractors for the performance of any contract we enter into with them or you; and</li>
          <li>analytics and search engine providers that assist us in the improvement and optimisation of the Website.</li>
        </ul>
    </p>  
    <p>We may share some broader statistics and customer profiling information with third parties and other Octopus companies, but the data is anonymised, so you would not be identifiable from that data. We will not rent or sell our user's details to any other organisation or individual.
    </p>
    <div class="title">STORAGE OF YOUR PERSONAL DATA</div>
    <p>The data that we collect from you may be transferred to, and stored at, a destination outside of the European Economic Area (“EEA”). It may also be processed by staff operating outside the EEA who work for us or one of our suppliers. By using the Website, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy.</p>
    <p>We follow strict security procedures as to how your personal information is stored and used, and who sees it, to help stop any unauthorised person getting hold of it. All personal information you register on this Website will be located behind a firewall. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. Unfortunately, the transmission of information via the internet is not completely secure and although we do our best to protect your personal data, we cannot guarantee the security of your data. So we cannot accept any liability for the loss, theft or misuse of the personal information which you have registered on the Website if there is a security breach. </p>
    <p> We will keep your information stored on our systems for as long as it takes to provide the services to you. The third parties we engage to provide services on our behalf will keep your data stored on their systems for as long as is necessary to provide the services to you. We will not store your information for longer than is reasonably necessary or required by law. </p>
    <div class="title">YOUR RIGHTS</div>
    <p>Even if you have accepted the processing of your personal data for marketing purposes (by ticking the relevant box), you have the right to ask us to stop processing your personal data for such purposes. You can exercise this right at any time by contacting us at digital@octopusgroup.com or +44 800 316 2295. </p>
    <p>The Website may, from time to time, contain links to and from the websites of advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites. </p>
    <p>From 25 May 2018, you will be entitled to receive the personal data that you have provided to us in a structured, commonly used and machine-readable format, and to transmit that data to another data controller. The types of data that can be provided are those set out at paragraph 3.1 above. You can exercise this right from 25 May 2018 by contacting us at digital@octopusgroup.com or +44 800 316 2295.</p>
    <div class="title">ACCESS TO INFORMATION</div>
    <p>European Data Protection Legislation gives you the right to access information held about you. You are entitled to be told by us whether we or someone else on our behalf is processing your personal information; what personal information we hold; details of the purposes for the processing of your personal information; and details of any third party with whom your personal information has been shared. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you. Sometimes you may be asked to provide proof of identity before we show you your personal information - that's so we can prevent unauthorised access. Your rights to access, rectify such information, object or cancel the information we hold about you can be exercised at any time by contacting us at digital@octopusgroup.com or +44 800 316 2295.</p>
    <div class="title">CONTACT</div>
    <p>Questions, comments and requests regarding this Privacy Policy should be addressed to:</p>
    <p>
        <div class="adress">
          <div>Address</div>
          <div>
                Digital @ Group Marketing
                <br>
                Octopus Group
                <br>
                33 Holborn
                <br>London
                <br>EC1N 2HT
                <br>UK
          </div>      
        </div>  
    </p>
    <p>Email address: 	<a href="mailto:digital@octopusgroup.com">digital@octopusgroup.com</a> </p>
    <p>Telephone number: +44 (0)20 3142 4906</p>
    <p>Alternatively, you can contact us through the “Contact Us” section of this Website.</p>
    <div class="title">DEFINITIONS</div>
    <p>
      For the purposes of this Privacy Policy, ("<b>European Data Protection Legislation</b>") is defined as, for the periods in which they are in force, 
      the European Data Protection Directive 95/46/EC, all laws giving effect or purporting to give effect to the European Data Protection Directive 95/46/EC 
      (such as the Data Protection Act 1998) or otherwise relating to data protection (to the extent the same apply) and, from 25 May 2018, 
      the General Data Protection Regulation (Regulation (EU) 2016/670) (GDPR) or any equivalent legislation amending or replacing the GDPR.
    </p>
</div>
<?php get_footer();
<?php 
    /* Template Name: Modern */ 
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<h1>Modern Slavery Act 2015</h1>
<div class="main policy">
<p>
  This statement is made as a result of the enactment of the Modern Slavery Act 2015.
</p>
<p>
  Octopus Capital Limited is a private limited company, incorporated in England and Wales with registered number 3981143. The Registered Office is at 33 Holborn, London EC1N 2HT (Octopus, We or Us).  Octopus Investments, a subsidiary company, is regulated by the Financial Conduct Authority.  Octopus has offices in England and New York only.
</p>
<div class="title">Services and Supply</div>
<p>
  The services Octopus utilises itself are office based and its supply chain in relation to services consists on the whole of other regulated professional services (Chartered Surveyors, Law Firms, Accountancy Firms and Banks). Octopus considers these to be very low risk in relation to slavery and human trafficking so takes no specific action in respect to these relationships.
</p>
<p>
  Octopus provides a range of services within the structure of investment and fund management.  Investment is made directly into energy generation and supply, healthcare, schools and renewable energy.  Octopus Investments also invests, on behalf of its customers, in a range of different businesses and industries through its Ventures and Specialist Finance Teams, these will include construction of housing, schools and hospitals, delivery of tech solutions to every day services (such as entertainment, property and social), food distribution, private jet services, chauffeur services, distribution of healthcare products.
</p>
<p>
  The majority of other services supplied to or on behalf of Octopus are from the construction industry and other services associated with the property (domestic, commercial, rural and forestry). Given what Octopus understands to be a low risk profile of anyone supplying us with services being involved in slavery and/or human trafficking, we believe our current procedures and ability to rely on regulatory oversight in relation to professional services are sufficient in this regard.
</p>
<div class="title">Goods</div>
<p>
  In terms of goods supplied to Octopus, the majority of goods will be goods for use in an office environment. Given what Octopus understands to be a low risk profile of suppliers of goods, no specific training is provided or undertaken by Octopus staff in relation to these matters.
</p>
<div class="title">Training of Staff</div>
<p>
  Octopus provides employees and management with training on a range of compliance matters which would include their obligations under the Modern Slavery Act 2015, particularly in relation to mitigating risks within supply chains.
</p>
<p>
  <img src="./assets/images/sign.svg" alt="Sign">
</p>
<p>M Cooper</p>
<p>Chairman</p>
<p>5 May 2016</p>
<p>Our products place your capital at risk and you may not get back the full amount invested.</p>
</div>
<?php get_footer();
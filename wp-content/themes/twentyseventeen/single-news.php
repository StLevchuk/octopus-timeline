<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
//include 'other_menu.php';

function one_tag( $text ) {
	return '<span>' . $text . '</span>';
}

function filter_tags( $tags ) {
	if ( ! $tags ) {
		return '<span>No tags</span>';
	}

	$generated_tags = '';

	$result = explode( '|', preg_replace( '/(,\s)/', '|', $tags ) );

	foreach ( $result as $tag ) {
		$generated_tags .= one_tag( $tag );
	}

	return $generated_tags;
}


$author   = get_post_custom_values( 'author', $post->ID )[0];
$image    = get_the_post_thumbnail_url();
$category = get_post_custom_values( 'category', $post->ID )[0];
$tags     = get_post_custom_values( 'tags', $post->ID )[0];

?>
	<div class="site-content-contain">
	<div id="content" class="site-content">
		<div class="main-image main-image_thumb"
				 style="background-image: url('<?php echo $image ? $image : "./assets/images/one_news/default.png"; ?>');">
			<img src="<?php echo $image ? $image : "./assets/images/one_news/default.png"; ?>"
					 alt="<?php echo $post->post_title; ?>">
		</div>
		<div class="header header-news">
			<div class="news_inner">
			<div class="author">
				<div class="name"><?php echo $author; ?></div>
				<div class="date"><?php echo date( "d F Y", strtotime( $post->post_date ) ); ?></div>
			</div>
			<div class="title">
				<?php echo $post->post_title; ?>
			</div>
			<div class="category">
				<span>Category:</span> <?php echo isset( $category ) ? $category : '-'; ?>
			</div>
			</div>
		</div>
		<div class="news">
			<div class="news_inner">
<!--			<div class="title">--><?php //echo $post->post_title; ?><!--</div>-->
			<p>
				<?php echo $post->post_content; ?>
			</p>
			<div class="tags">
				<?php
				if ( isset( $tags ) ) {
					echo filter_tags( $tags );
				}
				?>
			</div>
		</div>
		</div>
		<div class="footer">
			<div class="news_inner">
				<div class="author">
					<div class="title">Author</div>
					<div><?php echo $author; ?></div>
				</div>
				<div class="back-to-news">
					<div class="title">&nbsp;</div>
					<a href="/news">Back to news feed</a>
				</div>
			</div>
		</div>
		<h1>Related news</h1>
		<main class="main-news" role="main">
			<div class="news_grid">
				<?php
				$args = array(
						'post_type'      => 'news',
						'posts_per_page' => 3,
						'post__not_in'   => array( $post->ID ),
						'meta_query'     => array(
								array(
										'key'     => 'author',
										'value'   => $author,
										'compare' => 'LIKE',
								),
						)
				);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					$author = get_post_custom_values( 'author' )[0];
					?>
					<div class='news_cell'>
						<a href="<?php echo get_permalink(); ?>" class="item">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Octopus Group"/>
							<div class="main">
								<div class="data">
									<div class="main-content">
										<div class="name"><?php echo $author; ?></div>
										<div class="date-item"><?php echo get_the_time( 'd.m.Y' ); ?></div>
									</div>
								</div>
								<span class="title"><?php the_title(); ?></span>
								<p class="content"><?php echo limit( wp_strip_all_tags( get_the_content() ), 90 ); ?></p>
								<span class="read-more">
									Read More
									<span class="read-more-arrow"></span>
								</span>
							</div>
						</a>
					</div>
					<?php
				endwhile;
				?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer();

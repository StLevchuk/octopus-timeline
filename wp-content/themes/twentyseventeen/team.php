<?php 
    /* Template Name: Team */ 
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<h1>Our leadership team</h1>
<div class="teams">
  <div> 
    <div class="item">
      <div class="head">
          <div class="photo-profile chief-executive">
          </div>
          <div class="description">
            <div class="name">Simon Rogerson</div>
            <div class="position">Founder and Chief Executive Officer</div>
            <div class="department">Octopus Group and Octopus Investments</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Simon has overall responsibility for day-to-day decisions and for the strategic direction of the Group.
         Before founding Octopus in 2000, Simon spent two years working at Mercury Asset Management. 
         He graduated with a first class MA in Modern Languages from St.
        Andrews University and is a Chartered Financial Analyst.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile finance">
          </div>
          <div class="description">
            <div class="name">Carla Stent</div>
            <div class="position">Chief Financial Officer</div>
            <div class="department">Octopus Group</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Carla joined Octopus in 2016 and is responsible for shaping the Group’s operational strategy. Carla has operated at Board level in several countries for organisations including Virgin Group, Barclays Bank and Thomas Cook. Carla was born and educated in South Africa and is a Qualified Chartered Accountant. She is also a non-executive director on several boards and sub-committees including JPM Morgan Elect, the Post Office, Marex Spectron and Power to Change Trust.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile investment">
          </div>
          <div class="description">
            <div class="name">Alistair Seabright</div>
            <div class="position">Head of Unquoted Investments</div>
            <div class="department">Octopus Group</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Alistair oversees the unquoted investment businesses, having joined Octopus in 2010. He also sits on the Board of the Octopus Group. Alistair’s financial services experience includes ten years at SG Warburg (now UBS) in London, Tokyo and South Africa, managing an alternative asset management business investing in Japan, and co-founding an agency broking business for the Man Group. He has also worked for Chase Manhattan Bank, Phillips & Drew and James Capel. Alistair graduated from Cambridge University and has been a board member of several private companies.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile healthcare">
          </div>
          <div class="description">
            <div class="name">Mike Adams</div>
            <div class="position">Chief Executive Officer</div>
            <div class="department">Octopus Healthcare</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Mike joined Octopus Healthcare (previously known as MedicX) in 2005. He began his career with St Quintin before working for KPMG Property Consulting, Ernst & Young and MEPC plc. He left MEPC in 1999 to become a director of Stonemartin plc, responsible for its property portfolio. Mike is a member of the Royal Institution of Chartered Surveyors and has over 25 years’ experience in the UK commercial property market.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile ventures">
          </div>
          <div class="description">
            <div class="name">Alex Macpherson</div>
            <div class="position">Chief Executive Officer</div>
            <div class="department">Octopus Ventures</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Alex takes overall responsibility for investments made by Octopus Ventures. He founded Katalyst Ventures in 2000, creating the private angel network which would later be known as the Octopus Venture Partners. Before Katalyst, Alex worked for ten years at SG Warburg as a trade and risk manager in equity derivatives. Alex is also chairman of the Octopus Ventures Investment Committee, is a non-executive director of portfolio company Calastone, and a board member of Graze.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile energy">
          </div>
          <div class="description">
            <div class="name">Greg Jackson</div>
            <div class="position">Chief Executive Officer</div>
            <div class="department">Octopus Energy</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Greg is the founder of Octopus Energy. Previously, he built and sold e-commerce company C360 and built HomeServe's innovation business. Greg is also a non-executive director of Zopa, Consultant Connect and an investor in several technology start-ups. Greg started his working life as a video games programmer before graduating with a degree in Economics from Cambridge University.
      </div>
    </div>
  </div>
  <div>
    <div class="item">
      <div class="head">
          <div class="photo-profile founder">
          </div>
          <div class="description">
            <div class="name">Chris Hulatt </div>
            <div class="position">Founder</div>
            <div class="department">Octopus Group</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Chris is responsible for the Group’s expansion into new areas. He is also on the investment committee of several Octopus funds. Before founding Octopus in 2000, Chris led one of the global equity research teams at Mercury Asset Management. He has a first class MA in Natural Sciences from Cambridge University and is a Chartered Financial Analyst.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile marketing">
          </div>
          <div class="description">
            <div class="name">Seb Dreyfus</div>
            <div class="position">Chief Marketing Officer</div>
            <div class="department">Octopus Group</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Seb joined Octopus in 2015 and leads the strategic direction of the Octopus brands, having previously held senior roles at leading creative and digital agencies including Saatchi & Saatchi, Publicis Modem and Razorfish. Seb has a diploma in Art & Design from Bristol College, and a BA in English and Drama from the University of Toronto.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile manager">
          </div>
          <div class="description">
            <div class="name">Paul Latham</div>
            <div class="position">Managing Director</div>
            <div class="department">Octopus Investments</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Paul joined Octopus 2005 and is part of the Executive Committee for the Octopus Group.
        He spends much of his time focused on tax-efficient investments. 
        He is also the Chief Executive of Fern Trading Ltd, the UK’s largest commercial-scale producer of solar energy. 
        Paul had extensive general management and internal consulting experience gained in companies including Brakes, Capital One, NatWest and Kingfisher.
        He graduated with a first class honours degree from Imperial College
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile deputy">
          </div>
          <div class="description">
            <div class="name">Benjamin Davis</div>
            <div class="position">Deputy Chief Executive Officer</div>
            <div class="department">Octopus Healthcare</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Benjamin joined Octopus in 2010 and is responsible for the management of Octopus Healthcare. He previously led Octopus’ Intermediate Capital investment team and had responsibility for its Specialist Finance Venture Capital Trust product line. Prior to Octopus, he worked at YFM Equity Partners and was an associate director at the investment bank Interregnum, working across the corporate finance and venture capital investment teams in the UK and US. He has also worked as a consultant for blue chip companies in the UK and New Zealand. Benjamin has an MBA from the University of Cambridge and a BCom (Hons) degree from the University of Auckland, New Zealand.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile property">
          </div>
          <div class="description">
            <div class="name">Mario Berti</div>
            <div class="position">Chief Executive Officer</div>
            <div class="department">Octopus Property</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Mario joined Octopus in 2010 as Head of Specialist Finance and became CEO of Octopus Property in May 2016.  Mario is also Chief Investment Officer for the Unquoted division of Octopus Investments.  Before joining Octopus, Mario was at Rothschild for nine years, where he was a board member of NM Rothschild Banking.  Mario is a Qualified Chartered Accountant and read Philosophy, Politics and Economics at Oxford University.
      </div>
    </div>
    <div class="item">
      <div class="head">
          <div class="photo-profile labs">
          </div>
          <div class="description">
            <div class="name">Richard Wazacz </div>
            <div class="position">Chief Executive Officer</div>
            <div class="department">Octopus Labs</div>
          </div>
      </div>
      <div class="line">&nbsp;</div>
      <div class="body">
        Richard joined Octopus in December 2014 and is the founder of Octopus Labs. Previously he held senior roles at both Prudential and Lloyds Banking Group. Richard graduated from Cambridge University with a first-class honours degree in Chemical Engineering.  He also holds an MBA from Columbia Business School where he studied as a Fulbright Scholar and a Sainsbury Management Fellow.
      </div>
    </div>
  </div>
</div>
<?php get_footer();
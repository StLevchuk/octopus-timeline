<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<script>(function (w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(), event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
					j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
					'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-PRVX9JG');</script>
	<!-- End Google Tag Manager -->
	<meta name="format-detection" content="telephone=no">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
	set_meta_description();
	?>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<base href="/wp-content/themes/twentyseventeen/">
	<link rel="shortcut icon" href="/wp-content/themes/twentyseventeen/assets/images/icon.ico" type="image/x-icon">


<!--	<link rel="stylesheet" href="/wp-content/themes/twentyseventeen/main.css" type="text/css" media="all">-->
	<link rel="stylesheet" href="/wp-content/themes/twentyseventeen/style.css" type="text/css" media="all">
	<!-- <link rel="stylesheet" href="https://cdn.plyr.io/2.0.13/plyr.css"> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.3.6/mobile-detect.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
	<script
			src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-annotation/0.5.5/chartjs-plugin-annotation.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script
			src="https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.js"></script>

	<script src="/wp-content/themes/twentyseventeen/assets/js/Video.js.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/ScrollMagic.min.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/helper.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/menu.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/mobile-menu.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/sub-menu.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/mobile-slider.js"></script>
	<script
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMSkJC4deMmzYIvaCVJFg8Ohvhx1F9S5c">
	</script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/google-map.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/scrollable.js"></script>
	<script src="/wp-content/themes/twentyseventeen/assets/js/scrollDown.js"></script>
	<?php
	if ( is_home() ) {
		echo '<title>Octopus Group</title>';
	}
	if ( get_post_type() == 'news' || is_singular( 'news' ) ) {
		echo '<title>News and Insights | Octopus Group</title>';
	}
	?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PRVX9JG"
					height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site <?php echo ( is_front_page() || is_home() ) ? '' : ' no_home' ?> <?php echo wp_is_mobile() ? 'mobile' : '' ?>">

<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
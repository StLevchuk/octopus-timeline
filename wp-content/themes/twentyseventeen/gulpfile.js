'use strict';

const gulp = require('gulp');

function lazyRequireTask(taskName, path, options) {
  options = options || {};
  options.taskName = taskName;
  gulp.task(taskName, function(callback) {
    let task = require(path).call(this, options);

    return task(callback);
  });
}

lazyRequireTask('styles', './gulp-tasks/styles', {
  src: 'frontend/scss/*.scss',
	dst: './'
});

gulp.task('build', gulp.series('styles'));

gulp.task('watch', function() {
  gulp.watch('frontend/scss/**/*.scss', gulp.series('styles'));
});

gulp.task('dev',
		gulp.series('build' , gulp.parallel('watch'))
);


<div class="item form">
	<form name="search" class="search">
		<label for="search">Keyword search</label>
		<p class="search-block">
			<input type="text" name="search" id="search" placeholder="Financial news" <?php if($search_string) echo 'value="'.$search_string.'"' ?> >
			<input type="submit" value="Search">
		</p>
		<label for="sort">Sort by</label>
		<p>
			<select id="sort" name="sort">
				<option value="newest" <?php if((!$sort) || ($sort == 'newest')) echo 'selected'; ?> >Newest-oldest</option>
				<option value="oldest" <?php if($sort == 'oldest') echo 'selected'; ?> >Oldest-newest</option>
			</select>
		</p>
		<p>
			<label for="business">Business</label>
			<select id="business" name="business">
				<option value="" disabled <?php if(!$business) echo 'selected'; ?> >Select business</option>
				<option value="investments" <?php if($business && ($business == 'investments')) echo 'selected'; ?> >Octopus Investments</option>
				<option value="ventures" <?php if($business && ($business == 'ventures')) echo 'selected'; ?>>Octopus Ventures</option>
				<option value="property" <?php if($business && ($business == 'property')) echo 'selected'; ?>>Octopus Property</option>
				<option value="healthcare" <?php if($business && ($business == 'healthcare')) echo 'selected'; ?>>Octopus Healthcare</option>
				<option value="energy" <?php if($business && ($business == 'energy')) echo 'selected'; ?>>Octopus Energy</option>
				<option value="labs" <?php if($business && ($business == 'labs')) echo 'selected'; ?>>Octopus Labs</option>
			</select>
		</p>
		<label for="category">Category</label>
		<div>
			<input type="checkbox" name="category" value="insight" id="category" <?php if($insight_search_active) echo 'checked'; ?> >
			<label for="category"><span></span>Insight</label>
		</div>
		<div>
			<input type="checkbox" name="category" value="news" id="news" <?php if($news_search_active) echo 'checked'; ?>>
			<label for="news"><span></span>News</label>
		</div>
	</form>
</div>
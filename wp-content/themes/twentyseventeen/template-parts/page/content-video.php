<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('video-page'); ?>>
	<div class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div><!-- .entry-header -->
	<div class="entry-content">

		<section class="section video-section">
			<div class="section_inner">

				<div class="video-player-wrapper js-player-wrapp">
					<video id="video-payer" class="video-player" width="100%" controls="false" preload="auto" crossorigin>

						<source src="<?php echo get_template_directory_uri() ?>/assets/video/Octopus_Murmuration_3pm_30sec-compressed.mp4" type="video/mp4">
						<source src="<?php echo get_template_directory_uri() ?>/asstes/video/Octopus_Murmuration_3pm_30sec.webmhd.webm" type="video/webm">

<!--						<source src="//storage.googleapis.com/octopus-wordpress-group.appspot.com/Octopus_Murmuration_3pm_30sec.mp4" type="video/mp4">-->
<!--						<source src="//storage.googleapis.com/octopus-wordpress-group.appspot.com/Octopus_Murmuration_3pm_30sec.webmhd.webm" type="video/webm">-->
					</video>
					<div class="video-player_btn js-play-btn">
						<div class="video-player_play-icon"></div>
<!--						<div class="video-player_play-text">Watch our film</div>-->
					</div>
				</div><!--video-player-wrapper-->
			</div>
		</section>

		<?php
		//the_content();
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<header
		class="top-header head-block js-fixed-nav <?php echo ( is_front_page() || is_home() ) ? 'main-menu' : 'other-menu is-fixed'; ?>">
	<?php if ( is_front_page() || is_home() ): ?>
		<div class="logo">
			<img class="title logo-img--white" src="<?php echo get_template_directory_uri() ?>/assets/images/octopus_logo_white.png"/>
			<img class="title logo-img--origin" src="<?php echo get_template_directory_uri() ?>/assets/images/octopus_logo.png"/>
		</div>
	<?php else: ?>
		<div class="logo">
			<a href="<?php echo home_url( '/' ); ?>">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/octopus_logo.png" alt="">
			</a>
		</div>
	<?php endif; ?>

	<ul id="menu" class="menu">
		<li>
			<a href="/a-brighter-way">A brighter way</a>
			<div class="item-border"></div>
		</li>
		<li class="container">
			<a class="menu-item">Discover Octopus</a>
			<div class="item-border"></div>
			<div class="sub-menu hide">
				<div class="triangle"></div>
				<ul class="menu-list">
					<li><a href="/discover-octopus/about-us">About us</a></li>
					<li class="center"><a href="/discover-octopus/leadership-team">Our leadership team</a></li>
					<li><a href="/discover-octopus/octopus-giving">Octopus Giving</a></li>
				</ul>
			</div>
		</li>
		<li>
			<a href="/our-businesses">Our businesses</a>
			<div class="item-border"></div>
		</li>
		<li>
			<a href="/news">News and insights</a>
			<div class="item-border"></div>
		</li>
		<li>
			<a href="/careers">Careers</a>
			<div class="item-border"></div>
		</li>
		<li>
			<a href="/contact-us">Contact us</a>
			<div class="item-border"></div>
		</li>
	</ul>
</header>

<!--mobile menu?-->
<div class="wrapper-mobile-menu">
	<div class="mobile-header">
		<?php if ( is_front_page() || is_home() ): ?>
			<div class="mobile-logo"></div>
		<?php else: ?>
			<a href="<?php echo home_url( '/' ); ?>" class="mobile-logo"></a>
		<?php endif; ?>
		<div id="mobile-icon-menu" class="mobile-icon-menu"></div>
	</div>
	<div class="mobile-menu">
		<div id="close" class="close"></div>
		<?php if ( is_front_page() || is_home() ): ?>
			<div class="title">
				<img src="./assets/images/logo_white.svg" alt="">
			</div>
		<?php else: ?>
			<a href="<?php echo home_url( '/' ); ?>" class="title">
				<img src="./assets/images/logo_white.svg" alt="">
			</a>
		<?php endif; ?>
<!--		<div class="title">-->
<!--			<img src="./assets/images/logo_white.svg" alt="">-->
<!--		</div>-->
		<ul class="menu-list">
			<li><a href="/a-brighter-way">A brighter way</a></li>
			<li><a href="/discover-octopus/about-us">About us</a></li>
			<li><a href="/discover-octopus/leadership-team">Our leadership team</a></li>
			<li><a href="/discover-octopus/octopus-giving">Octopus giving</a></li>
			<li><a href="/our-businesses">Our businesses</a></li>
			<li><a href="/news">News and insights</a></li>
			<li><a href="/careers">Careers</a></li>
			<li><a href="/contact-us">Contact Us</a></li>
		</ul>
	</div>
</div>
<?php
$author = get_post_custom_values('author', $post->ID)[0];
$photo = get_post_custom_values('photo', $post->ID)[0];

$thumbnail = get_the_post_thumbnail_url($post);
$time = get_the_time('d.m.Y');

$title = get_the_title($post->ID);
$content = limit(wp_strip_all_tags($post->post_content), 90);
$permalink = get_permalink();
?>
<a href="<?php echo $permalink; ?>" class="item">
	<img src="<?php echo $thumbnail; ?>" alt="" />
	<div class="main">
		<div class="data">
			<div class="main-content">
				<div class="name"><?php echo $author; ?></div>
				<div class="date-item"><?php echo $time; ?></div>
			</div>
		</div>
		<span class="title"><?php echo $title; ?></span>
		<p class="content"><?php echo $content; ?></p>
		<span class="read-more">
			Read More
			<span class="read-more-arrow"></span>
		</span>
	</div>
</a>
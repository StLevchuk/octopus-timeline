<?php 
    /* Template Name: Giving */
    get_header(); 
?>
<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<!--<div class="line">&nbsp;</div>-->
<div id="content" class="giving site-content">
<h1>Octopus Giving</h1>
<div class="wrapper">
  <div class="left">
    <div class="image-block">
      <img src="./assets/images/giving/giving.png">
    </div>
  </div>
  <div class="right">
    <p>
      When we first started out, we relied on the small number of people who gave us a chance and chose to invest in our dream. We never forget that. Octopus Giving is our way of putting something back, by helping to give others their chance.
    </p>
    <p>
      The charities we support started out small, like us. But they have big ambitions. And they help people who could very easily slip through the cracks without anyone there to make a difference.
    </p>
    <p>
      Since it launched in 2014, Octopus Giving has donated almost £700,000 to our charities. But it’s not just about money. We try to give our charities something even more valuable – our time.
    </p>
    <p>
       Everyone who works at Octopus spends a ‘volunteer day’ each year with one of our charities. So far we’ve donated more than 167 volunteer days this year, and we’ve pledged another 292 days for the rest of the year.
    </p>
    <p class="contacts">For more information, contact 
      <a>giving@octopusinvestments.com</a>
    </p>
  </div>
</div>
<div class="charities">
  <div class="title">Our charities</div>
    <div class="parent">
      <div class="left-column">
        <div class="charity">
          <div class="title">
            Campaign Against Living Miserably (CALM)
          </div>
          <div class="body">
            In 2015, 75% of all UK suicides were male. CALM is an award-winning charity dedicated to preventing the single biggest killer of men under the age of 45 in the UK. With a website and helpline, CALM is challenging a culture that prevents men seeking help when they need it. 
          <div class="line">&nbsp;</div>
          </div>
          <div class="footer calm">
            <div class="address"><a href="https://thecalmzone.net" target="_blank">thecalmzone.net</a></div>
            <div class="link"><a href="https://twitter.com/theCALMzone" target="_blank">@theCALMzone</a></div>
          </div>
        </div>
        <div class="charity">
          <div class="title">
            Youth at Risk
          </div>
          <div class="body">
            Youth at Risk offers intensive coaching and support to some of the UK’s most vulnerable young people and the adults supporting them. They believe all young people, no matter what their start in life, can be inspired to turn their lives around. 
          <div class="line">&nbsp;</div>
          </div>
          <div class="footer youthatrisk">
            <div class="address"><a href="http://youthatrisk.org.uk/" target="_blank">youthatrisk.org.uk</a></div>
            <div class="link"><a href="https://twitter.com/YouthatRiskUK" target="_blank">@YouthatRiskUK</a></div>
          </div>
        </div>
        <div class="charity">
          <div class="title">
            North London Cares
          </div>
          <div class="body">
            Between them, North London Cares and South London Cares are tackling the growing problem of loneliness and isolation for elderly people living in the capital. Their network of volunteers offers a little extra time, practical help and human companionship. 
          <div class="line">&nbsp;</div>
          </div>
          <div class="footer north-london">
            <div class="address"><a href="https://northlondoncares.org.uk" target="_blank">northlondoncares.org.uk</a></div>
            <div class="link"><a href="https://twitter.com/NorthLDNCares" target="_blank">@NorthLDNCares</a></div>
          </div>
        </div>
      </div>
      <div class="right-column">
        <div class="charity">
          <div class="title">
            Greatwood Charity
          </div>
          <div class="body">
            Greatwood uses ex-racehorses to educate disadvantaged children and young adults with special educational needs. As well as delivering pioneering education programmes designed to teach emotional literacy and life skills, it is the only racehorse welfare charity that does not have a criteria for entry, and they never turn away a horse that is suffering or neglected.
          <div class="line">&nbsp;</div>
          </div>
          <div class="footer greatwood">
            <div class="address"><a href="http://www.greatwoodcharity.org/" target="_blank">greatwoodcharity.org</a></div>
            <div class="link"><a href="https://twitter.com/GreatwoodHorses" target="_blank">@GreatwoodHorses</a></div>
          </div>
        </div>
        <div class="charity">
          <div class="title">
            Beyond Autism
          </div>
          <div class="body">
            Children and families living with autism face huge challenges but research shows ‘verbal behaviour’ is the most successful way to help them make real breakthroughs. Beyond Autism provides autistic children with the environment and communication skills they need to lead fuller, happier lives.
          <div class="line">&nbsp;</div>
          </div>
          <div class="footer autism">
            <div class="address"><a href="http://www.beyondautism.org.uk/" target="_blank">beyondautism.org.uk</a></div>
            <div class="link"><a href="https://twitter.com/BeyondAutismUK" target="_blank">@BeyondAutismUK</a></div>
          </div>
        </div>
        <div class="charity">
          <div class="title">
            South London Cares
          </div>
          <div class="body">
            Operating in the boroughs of Southwark and Lambeth, South London Cares runs projects that break down many of the modern-day barriers to social interaction, to bring communities closer together. All of its activities aim to make older neighbours feel more valued, vibrant and visible. 
          <div class="line">&nbsp;</div>
          </div>
          <div class="footer south-london">
            <div class="address"><a href="https://southlondoncares.org.uk" target="_blank">southlondoncares.org.uk</a></div>
            <div class="link"><a href="https://twitter.com/SouthLDNCares" target="_blank">@SouthLDNCares</a></div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php get_footer();
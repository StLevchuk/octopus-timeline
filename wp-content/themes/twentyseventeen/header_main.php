<section class="hero">
	<?php //include 'other_menu.php'; ?>
	<?php
	if ( is_front_page() || is_home() ):
		if ( ! wp_is_mobile() ):
			include( 'video.php' );
		else:
			include( 'mobile_video.php' );
		endif;
	endif;
	?>
	<div class="main-block">
		<div class="title-one">Better</div>
		<div class="title-two">It's something everyone wants</div>
		<div class="play-video js-play"></div>
		<div class="play-video-text">Play Video</div>
	</div>
	<div class="scroll-down">
		<div class="text">Scroll Down</div>
		<img src="/wp-content/themes/twentyseventeen/assets/images/arrow_down.png" alt="">
	</div>
</section>
<div class="site-content-contain">
	<div id="content" class="site-content">
<?php 
    /* Template Name: About us */ 
    get_header(); 
?>

<?php //include 'other_menu.php'; ?>
<div class="site-content-contain">
<div id="content" class="site-content">
<h1>About us</h1>
<div class="item-odd first">
  <div class="left">
    <div class="title">Being different can be difficult</div>
      <div class="body">
        <div class="line">&nbsp;</div>
        <p>
          That’s how it felt for our three founders. <br>
          They wondered why companies weren’t doing more to earn the trust of their customers.</br>
          So they decided to build something different. Something better. From the ground up.
        </p>
        <p>
          But starting out meant having to work harder.</br>
          We had to show people <b>why</b> we were different. <br>
          And then explain why being different was a good thing for <b>them</b>.
          </br>That’s not so easy.
        </p>
      </div>
  </div> 
  <div class="right">
      <img src="./assets/images/aboutUs/about-us-first.jpg">
  </div>
  <div class="line-bottom">&nbsp;</div>
</div>
<div class="item-even">
    <div class="left">
      <img src="./assets/images/aboutUs/about-us-second.jpg">
    </div>
    <div class="right">
      <div class="title">
       You never forget your first
      </div>
      <div class="body">
        <div class="line">&nbsp;</div>
        <p>
           We never forget our first customer, Mr Gower. 
           <br>
           We were so pleased to have his business, we wrote to him.
           <br>
           To say thank you. 
        </p>
        <p>
           Mr Gower wrote back.
           <br>
           He told us we were the first company that had ever written simply to say thank you. 
           <br>
           It made him feel special.
           <br>
           And he liked that we were different.
        </p>
        <p>
          It was a first for us, too.
          <br> 
          Our first personal connection.
          <br> 
          It felt great. 
          <br>
          So, we decided to keep being different. 
        </p>
        <p>
          This meant going out of our way to put our customers first. 
          <br>
          It meant designing products that did what we said they would.
          <br> 
          Sticking to our promises and holding our hands up when we got things wrong. 
          <br>
          And above all, it meant never stop listening.
        </p>
      </div>
    </div>
  <div class="line-bottom">&nbsp;</div> 
</div>
<div class="item-odd last">
  <div class="left">
    <div class="title">Spreading health, wealth and happiness</div>
      <div class="body">
        <div class="line">&nbsp;</div>
        <p>
          Our ambition didn’t end with investments.
          <br>
          We wanted Octopus to make a difference in other industries as well.
          <br>
          Industries like healthcare and energy.
          <br>
          Where customers deserved better.
        </p>
        <p>
          Which is why, over the last few years, we’ve invested over £1 billion
          in hospitals, care homes and schools where you know your loved ones are in good hands.
          <br>
          And our energy business is the number one rated energy business in the country.
        </p>
        <p>
          Across all Octopus businesses, we now have 150,000* customers.
          <br>
          Customers who have entrusted us with almost £7 billion* of their money.
          <br>
          Each customer is different, but equally important to us.
       </p>
       <p>   
          We hope the businesses we build will go on to improve the lives of millions of people.
          <br>
          And that we create a company we’ll be proud to tell our grandchildren about.
          <br>
          <i>* Source: Octopus as at 31/03/2017</i>
        </p>
      </div>
  </div> 
  <div class="right">
      <img src="./assets/images/aboutUs/about-us-third.jpg">
  </div>
</div>
<?php  get_footer(); 
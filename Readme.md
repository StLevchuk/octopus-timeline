How to add news:
 1) Go to admin panel -> News -> Add New
 2) Enter information that you needed to title and text
 3) Add custom fields above text, you need to add tree items: 
 - `author` - This will be displayed as author of news
 - `category` - Category of news
 - `photo` - The url of main news photo
 - `tags` - List of tags for that news. Only format in example need to be provided, other formats not supported yet. Example: `Tag1, Tag2, Tag3`